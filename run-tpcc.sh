#!/bin/bash

set -eu

warehouses="50 100"
connections="20 40 60 80 100"

mkdir -p ${HOME}/log/tpcc

cd ${HOME}/tpcc-mysql

for num_wh in $warehouses; do
  database="tpcc${num_wh}"
  mysql -u${MYSQL_USER} -p${MYSQL_PASS} -h${MYSQL_HOST} -e "DROP DATABASE IF EXISTS ${database}"
  mysql -u${MYSQL_USER} -p${MYSQL_PASS} -h${MYSQL_HOST} -e "CREATE DATABASE ${database}"
  mysql -u${MYSQL_USER} -p${MYSQL_PASS} -h${MYSQL_HOST} ${database} < create_table.sql
  mysql -u${MYSQL_USER} -p${MYSQL_PASS} -h${MYSQL_HOST} ${database} < add_fkey_idx.sql
  ./tpcc_load -u ${MYSQL_USER} -p ${MYSQL_PASS} -h ${MYSQL_HOST} -d ${database} -w ${num_wh}

  for num_conn in $connections; do
    ./tpcc_start \
      -u ${MYSQL_USER} \
      -p ${MYSQL_PASS} \
      -h ${MYSQL_HOST} \
      -d ${database} \
      -w ${num_wh}  \
      -c ${num_conn} -r 300 -l 600 \
      | tee ${HOME}/log/tpcc/wh${num_wh}_conn${num_conn}.log
  done
done
