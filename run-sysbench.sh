#!/bin/bash

set -eu

table_sizes="10000 100000 1000000"
threads="2 4 8 16 32 64 128 256"

for size in $table_sizes; do
  database="sysbench${size}"
  mysql -u${MYSQL_USER} -p${MYSQL_PASS} -h${MYSQL_HOST} -e "DROP DATABASE IF EXISTS ${database}"
  mysql -u${MYSQL_USER} -p${MYSQL_PASS} -h${MYSQL_HOST} -e "CREATE DATABASE ${database}"

  sysbench /usr/share/sysbench/oltp_read_write.lua \
    --db-driver=mysql \
    --table-size=${size} \
    --mysql-host=${MYSQL_HOST} \
    --mysql-user=${MYSQL_USER} \
    --mysql-password=${MYSQL_PASS} \
    --mysql-db=${database} \
    prepare

  for num_th in $threads; do
    mkdir -p ${HOME}/log/sysbench/t${size}
    sysbench /usr/share/sysbench/oltp_read_write.lua \
      --db-driver=mysql \
      --table-size=${size} \
      --mysql-host=${MYSQL_HOST} \
      --mysql-user=${MYSQL_USER} \
      --mysql-password=${MYSQL_PASS} \
      --mysql-db=${database} \
      --threads=${num_th} \
      --time=300 \
      run \
      | tee ${HOME}/log/sysbench/t${size}/th${num_th}.log
  done
done
