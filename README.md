Cloud RDB Benchmark
===================

# Results

## tpcc-mysql

```
warehouses  = {50, 100}
connections = {20, 40, 60, 80, 100}
```

![tpcc-mysql-50](https://gitlab.com/nownabe/rdb-bench/raw/master/results/tpcc-mysql-50.png)

![tpcc-mysql-100](https://gitlab.com/nownabe/rdb-bench/raw/master/results/tpcc-mysql-100.png)

Ignored RDS for 100 warehouses test because RDS is too slow.

## sysbench

```
table_size = {10000, 100000, 1000000}
threads    = {2, 4, 8, 16, 32, 64, 128, 256}
```

![oltp_read_write-10k](https://gitlab.com/nownabe/rdb-bench/raw/master/results/oltp_read_write-10k.png)

![oltp_read_write-100k](https://gitlab.com/nownabe/rdb-bench/raw/master/results/oltp_read_write-100k.png)

![oltp_read_write-1m](https://gitlab.com/nownabe/rdb-bench/raw/master/results/oltp_read_write-1m.png)

# Build Databases

Make sure aws cli and gcloud are configured.
(Or configure their credentials.)

```bash
$ cd terraform
$ cp .envrc.example .envrc
$ vi .envrc
$ source .envrc
$ terraform init
$ terraform plan
$ terraform apply
```

# Benchmark Details

Configure MySQL credentials before tests.

```bash
$ echo "export MYSQL_USER=user"
$ echo "export MYSQL_PASS=password"
$ echo "export MYSQL_HOST=mysql-host"
```

## tpcc-mysql

### Preparation

Install tpcc-mysql.

```bash
$ sudo yum install -y gcc make git mysql-devel
$ git clone https://github.com/Percona-Lab/tpcc-mysql.git ${HOME}/tpcc-mysql
$ cd tpcc-mysql/src
$ make
```

Download test script.

```bash
$ curl -sSL -o ${HOME}/run-tpcc.sh https://gitlab.com/nownabe/rdb-bench/raw/master/run-tpcc.sh
```

Run tests.

```bash
$ bash ${HOME}/run-tpcc.sh
```

Then see `${HOME}/log/tpcc`.

## sysbench

Install sysbench.

```bash
$ curl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.rpm.sh | sudo bash
$ sudo yum -y install sysbench
```

Download test script.

```bash
$ curl -sSL -o ${HOME}/run-sysbench.sh https://gitlab.com/nownabe/rdb-bench/raw/master/run-sysbench.sh
```

Run tests.

```bash
$ bash ${HOME}/run-sysbench.sh
```

Then see `${HOME}/log/sysbench`.
