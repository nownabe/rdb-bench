provider "aws" {
  version = "~>1.25"
  region  = "ap-northeast-1"
}

provider "google" {
  version = "~>1.15"
}
