variable "public_key" {}
variable "rds_instance_class" {}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"

  tags {
    Name = "rdbbench"
  }
}

resource "aws_security_group" "ssh" {
  name = "rdbbench-ssh"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "rdbbench-ssh"
  }
}

resource "aws_security_group" "mysql" {
  name = "rdbbench-mysql"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["${aws_vpc.vpc.cidr_block}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "rdbbench-mysql"
  }
}

resource "aws_subnet" "subnet1" {
  availability_zone = "ap-northeast-1a"
  cidr_block        = "10.0.1.0/24"
  vpc_id            = "${aws_vpc.vpc.id}"

  tags {
    Name = "rdbbench"
  }
}

resource "aws_subnet" "subnet2" {
  availability_zone = "ap-northeast-1c"
  cidr_block        = "10.0.2.0/24"
  vpc_id            = "${aws_vpc.vpc.id}"

  tags {
    Name = "rdbbench"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "rdbbench"
  }
}

resource "aws_route_table" "route_table" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
}

resource "aws_route_table_association" "association1" {
  subnet_id      = "${aws_subnet.subnet1.id}"
  route_table_id = "${aws_route_table.route_table.id}"
}

resource "aws_route_table_association" "association2" {
  subnet_id      = "${aws_subnet.subnet2.id}"
  route_table_id = "${aws_route_table.route_table.id}"
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "rdbbench"
  subnet_ids = ["${aws_subnet.subnet1.id}", "${aws_subnet.subnet2.id}"]

  tags {
    Name = "rdbbench"
  }
}

resource "aws_db_instance" "rds" {
  allocated_storage      = 10
  db_subnet_group_name   = "${aws_db_subnet_group.db_subnet_group.name}"
  engine                 = "mysql"
  engine_version         = "5.7.21"
  instance_class         = "${var.rds_instance_class}"
  multi_az               = true
  password               = "password"
  skip_final_snapshot    = true
  storage_type           = "gp2"
  username               = "user"
  vpc_security_group_ids = ["${aws_security_group.mysql.id}"]
}

resource "aws_rds_cluster" "aurora" {
  cluster_identifier     = "rdbbench"
  master_password        = "password"
  master_username        = "user"
  skip_final_snapshot    = true
  vpc_security_group_ids = ["${aws_security_group.mysql.id}"]
  db_subnet_group_name   = "${aws_db_subnet_group.db_subnet_group.name}"
  engine                 = "aurora-mysql"
  engine_version         = "5.7.12"
}

resource "aws_rds_cluster_instance" "aurora" {
  identifier           = "rdbbench"
  cluster_identifier   = "${aws_rds_cluster.aurora.cluster_identifier}"
  instance_class       = "${var.rds_instance_class}"
  db_subnet_group_name = "${aws_db_subnet_group.db_subnet_group.name}"
  engine               = "aurora-mysql"
  engine_version       = "5.7.12"
}

resource "aws_key_pair" "key_pair" {
  key_name   = "rdbbench"
  public_key = "${var.public_key}"
}

resource "aws_instance" "client" {
  ami                         = "ami-e99f4896"
  instance_type               = "m5.xlarge"
  key_name                    = "${aws_key_pair.key_pair.key_name}"
  vpc_security_group_ids      = ["${aws_security_group.ssh.id}"]
  associate_public_ip_address = true
  subnet_id                   = "${aws_subnet.subnet1.id}"

  tags = {
    Name = "rdbbench"
  }
}
