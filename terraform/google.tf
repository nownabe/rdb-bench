variable "google_project_id" {}
variable "cloud_sql_tier" {}
variable "billing_account_name" {}

data "google_billing_account" "billing" {
  display_name = "${var.billing_account_name}"
  open         = true
}

resource "google_project" "project" {
  name            = "rdbbench"
  project_id      = "${var.google_project_id}"
  billing_account = "${data.google_billing_account.billing.id}"
}

resource "google_project_service" "compute" {
  service = "compute.googleapis.com"
  project = "${google_project.project.id}"
}

resource "google_compute_address" "address" {
  name    = "rdbbench"
  region  = "asia-northeast1"
  project = "${google_project.project.id}"
}

resource "google_sql_database_instance" "master" {
  region = "asia-northeast1"

  settings {
    tier             = "${var.cloud_sql_tier}"
    disk_autoresize  = true
    disk_size        = 100
    disk_type        = "PD_SSD"
    replication_type = "SYNCHRONOUS"

    backup_configuration {
      binary_log_enabled = true
      enabled            = true
    }

    ip_configuration {
      ipv4_enabled = true

      authorized_networks {
        name  = "rdbbench"
        value = "${google_compute_address.address.address}"
      }
    }
  }

  database_version = "MYSQL_5_7"
  name             = "master"
  project          = "${google_project.project.id}"
}

resource "google_sql_database_instance" "failover" {
  region = "${google_sql_database_instance.master.region}"

  settings {
    tier                   = "${google_sql_database_instance.master.settings.0.tier}"
    crash_safe_replication = true
    disk_autoresize        = true
    disk_size              = "${google_sql_database_instance.master.settings.0.disk_size}"
    disk_type              = "PD_SSD"
    replication_type       = "SYNCHRONOUS"
  }

  database_version     = "MYSQL_5_7"
  name                 = "failover"
  master_instance_name = "${google_sql_database_instance.master.name}"
  project              = "${google_project.project.id}"

  replica_configuration {
    failover_target = true
  }
}

resource "google_sql_user" "user" {
  name     = "user"
  instance = "${google_sql_database_instance.master.name}"
  password = "password"
  host     = "%"
  project  = "${google_project.project.id}"
}

resource "google_compute_instance" "client" {
  boot_disk {
    auto_delete = true

    initialize_params {
      size  = 20
      type  = "pd-standard"
      image = "centos-cloud/centos-7"
    }
  }

  machine_type = "n1-standard-4"
  name         = "rdbbench"
  zone         = "asia-northeast1-a"

  network_interface {
    network = "default"

    access_config {
      nat_ip = "${google_compute_address.address.address}"
    }
  }

  project = "${google_project.project.id}"
}
